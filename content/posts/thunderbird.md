---
title: "Thunderbird"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Mozilla Thunderbird is a free and open-source[8] cross-platform email client, personal information manager, news client, RSS and chat client that is operated by the Mozilla Foundation's subsidiary MZLA Technologies Corporation. Thunderbird is an independent, community-driven project that is managed and overseen by the Thunderbird Council, which is elected by the Thunderbird Community. The project strategy was originally modeled after that of Mozilla's Firefox web browser and is an interface built on top of that web browser.

