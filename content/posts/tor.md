---
title: "tor"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Tor, short for The Onion Router, is free and open-source software for enabling anonymous communication. It directs Internet traffic via a free, worldwide, volunteer overlay network that consists of more than seven thousand relays. Using Tor makes it more difficult to trace a user's Internet activity.