---
title: "Inkspace"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Inkscape is a free and open-source vector graphics editor for GNU/Linux, Windows and macOS. It offers a rich set of features and is widely used for both artistic and technical illustrations such as cartoons, clip art, logos, typography, diagramming and flowcharting. It uses vector graphics to allow for sharp printouts and renderings at unlimited resolution and is not bound to a fixed number of pixels like raster graphics. Inkscape uses the standardized Scalable Vector Graphics (SVG) file format as its main format, which is supported by many other applications including web browsers. 

