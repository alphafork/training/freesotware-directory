---
title: "kodi"
date: 2022-11-20T09:03:20-08:00
draft: true
---

## Introduction

Kodi (formerly XBMC) is a free and open-source media player and technology convergence software application developed by the Kodi Foundation, a non-profit technology consortium.[5] Kodi is available for multiple operating systems and hardware platforms, with a software 10-foot user interface for use with televisions and remote controls. It allows users to play and view most streaming media, such as videos, music, podcasts, and videos from the Internet, as well as all common digital media files from local and network storage media, or TV gateway viewer.[6]
