---
title: "Gcompris"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## 

GCompris is a software suite comprising educational entertainment software for children aged 2 to 10. GCompris was originally written in C and Python using the GTK+ widget toolkit, but a rewrite in C++ and QML using the Qt widget toolkit has been undertaken since early 2014. GCompris is free and open-source software and the current version is subject to the requirements of the AGPL-3.0-only license. It has been part of the GNU project.


It is available for Linux, BSD, macOS, Windows and Android. While binaries compiled for Microsoft Windows and macOS were initially distributed with a restricted number of activities and a small fee was required to unlock all the activities, since February 2020 the full version is entirely free for all platforms.



