---
title: "Scribus"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Scribus is free and open-source desktop publishing (DTP) software available for most desktop operating systems.
It is designed for layout, typesetting, and preparation of files for professional-quality image-setting equipment.
Scribus can also create animated and interactive PDF presentations and forms.
Example uses include writing newspapers, brochures, newsletters, posters, and books.

The Scribus 1.4 series are the current stable releases, and the 1.5 series where developments are made available in preparation for the next stable release series, version 1.6.[4]

Scribus is written in Qt and released under the GNU General Public License.
There are native versions available for Unix, Linux, BSD, macOS, Haiku, Microsoft Windows, OS/2 (including ArcaOS and eComStation) operating systems.

Scribus supports most major bitmap formats, including TIFF, JPEG, and PSD.
Vector drawings can be imported or directly opened for editing. The long list of supported formats includes Encapsulated PostScript, SVG, Adobe Illustrator, and Xfig. 
Professional type/image-setting features include CMYK colors and ICC color management.
It has a built-in scripting engine using Python.
It is available in 60 languages.