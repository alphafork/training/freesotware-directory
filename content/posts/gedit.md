---
title: "gedit"
date: 2023-10-07
draft: true
---
## Introduction


gedit

gedit
gedit 41.0 showing a web page with syntax highlighting and three tabs
Developer(s)	Paolo Maggi
Paolo Borelli
Steve Frécinaux
Jesse van den Kieboom
James Willcox
Chema Celorio
Federico Mena Quintero[citation needed]
Initial release	February 12, 1999; 24 years ago
Stable release	
44.2[1] Edit this on Wikidata / 19 January 2023; 8 months ago
Preview release	
43.alpha[2] Edit this on Wikidata / 6 July 2022; 14 months ago
Repository	
gitlab.gnome.org/GNOME/gedit/ Edit this at Wikidata
Written in	C, Python
Platform	Linux, MacOS
Type	Text editor
License	GPL-2.0-or-later
Website	wiki.gnome.org/Apps/Gedit
gedit (/ˈdʒɛdɪt/ or /ˈɡɛdɪt/)[3] is a text editor designed for the GNOME desktop environment. It was GNOME's default text editor and part of the GNOME Core Applications until GNOME version 42 in March 2022, which changed the default text editor to GNOME Text Editor.[4] Designed as a general-purpose text editor, gedit emphasizes simplicity and ease of use, with a clean and simple GUI, according to the philosophy of the GNOME project.[5] It includes tools for editing source code and structured text such as markup languages.

Visit the [Hugo](https://gohugo.io) website!

