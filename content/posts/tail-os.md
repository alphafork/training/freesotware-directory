---
title: "Tails-OS"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Tails, or "The Amnesic Incognito Live System," is a security-focused Debian-based Linux distribution aimed at preserving privacy and anonymity. It connects to the Internet exclusively through the anonymity network Tor. The system is designed to be booted as a live DVD or USB and never writes to the hard drive or SSD, leaving no digital footprint on the machine unless explicitly told to do so. It can also be run as a virtual machine, with some additional security risks. The Tor Project provided financial support for its development at the beginning of the project and continues to do so alongside numerous corporate and anonymous sponsors.

Visit the [Hugo](https://gohugo.io) website!

