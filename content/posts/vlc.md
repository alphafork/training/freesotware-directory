---
title: "My First Post"
date: 2022-11-20T09:03:20-08:00
draft: true
---
## Introduction
VLC media player (previously the VideoLAN Client and commonly known as simply VLC) is a free and open-source, portable, cross-platform media player software and streaming media server developed by the VideoLAN project. VLC is available for desktop operating systems and mobile platforms, such as Android, iOS and iPadOS. VLC is also available on digital distribution platforms such as Apple's App Store, Google Play, and Microsoft Store.

VLC supports many audio- and video-compression-methods and file-formats, including DVD-Video, Video CD, and streaming-protocols. It is able to stream media over computer networks and can transcode multimedia files

This is **bold** text, and this is *emphasized* text.

Visit the [Hugo](https://gohugo.io) website!

