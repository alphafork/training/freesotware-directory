---
title: "Gnu-guix-os"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

GNU Guix System or Guix System is a rolling release, free and open source Linux distribution built around the GNU Guix package manager. It enables a declarative operating system configuration and allows reliable system upgrades which the user can rollback. It uses the GNU Shepherd init system  and the Linux-libre kernel, with support of the GNU Hurd kernel under development. On February 3, 2015, the Free Software Foundation added the distribution to its list of endorsed free Linux distributions.The Guix package manager and the Guix System drew inspiration from and were based on the Nix package manager and NixOS respectively.



