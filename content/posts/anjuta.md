--
title: "Anjuta"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

Anjuta was an integrated development environment written for the GNOME project.[7] It had support for C, C++, Java, JavaScript, Python and Vala programming language.[8] In May 2022, the project was archived due a lack of maintainers.[9] Since October 2022 the project's former homepage no longer exists and the domain is owned by an SBOBET, an Indonesian gambling website. It has been superseded by GNOME Builder.

