---
title: "Linux Kernel"
date: 2022-11-20T09:03:20-08:00
draft: true
author: "ranjithsiji"
---
##  Linux kernel

The **Linux kernel** is a [free and open-source](https://en.wikipedia.org/wiki/Free_and_open-source_software "Free and open-source software"),  [monolithic](https://en.wikipedia.org/wiki/Monolithic_kernel "Monolithic kernel"), [modular](https://en.wikipedia.org/wiki/Modular_programming "Modular programming"), [multitasking](https://en.wikipedia.org/wiki/Computer_multitasking "Computer multitasking"), [Unix-like](https://en.wikipedia.org/wiki/Unix-like "Unix-like") operating system [kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system) "Kernel (operating system)"). It was originally written in 1991 by [Linus Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds "Linus Torvalds") for his [i386](https://en.wikipedia.org/wiki/Intel_80386 "Intel 80386")-based PC, and it was soon adopted as the kernel for the [GNU operating system](https://en.wikipedia.org/wiki/GNU "GNU"), which was written to be a [free (libre)](https://en.wikipedia.org/wiki/Free_software "Free software") replacement for [Unix](https://en.wikipedia.org/wiki/Unix "Unix").


## Details

 - Original author(s)	Linus Torvalds Developer(s)	Community contributors
 - Linus Torvalds Initial release	0.02 (5 October 1991; 32 years ago)
 - Stable release	   6.6-rc4[2] /1 October 2023 
 - Written in	C (C11 since 5.18, C89 before),[3] Rust (since 6.1), assembly language
 - Available in	English License	GPL-2.0-only with
 -  Linux-syscall-note Website	kernel.org
