---
title: "NixOS"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

**NixOS** is a Linux distribution built on top of the Nix package manager. Its declarative configuration allows reliable system upgrades via several official channels. NixOS has tools dedicated to DevOps and deployment tasks.

Features that distinguish NixOS from other Linux distributions include:

**Abstraction**: The software packages making up a system can be configured using the Nix language syntax.
**Reproducible systems**: A replica of a system can be created on another machine with one configuration file.
**Atomic upgrades**: System upgrades involve less risk of breakage, and if something does go wrong, it is simple to roll back to the previous state.
**Immutability**: The software making up a given system configuration cannot be changed once it has been built, preventing accidental or malicious modifications.
**Nix package manager**: Packages can be installed without affecting the rest of the system, and can be tested without installing.
