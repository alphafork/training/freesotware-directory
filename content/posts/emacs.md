---
title: "My First Post"
date: 2023-10-20T09:03:20-08:00
draft: true
---
## Introduction


Emacs has over 10,000 built-in commands and its user interface allows the user to combine these commands into macros to automate work. Implementations of Emacs typically feature a dialect of the Lisp programming language, allowing users and developers to write new commands and applications for the editor. Extensions have been written to, among other things, manage files, remote access,[9] e-mail, outlines, multimedia, Git integration, and RSS feeds,[10] as well as implementations of ELIZA, Pong, Conway's Life, Snake, Dunnet, and Tetris.[1

