---
title: "Audacity"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction
Audacity is a free and open-source digital audio editor and recording application software, available for Windows, macOS, Linux, and other Unix-like operating systems
As of December 6, 2022, Audacity is the most popular download at FossHub,[8] with over 114.2 million downloads since March 2015. It was previously served from Google Code and SourceForge, where it was downloaded over 200 million times.

