---
title: "Geary"
date: 2023-10-07T09:03:20-08:00
draft: true
---

## Introduction

Geary is a free and open-source email client written in Vala and based on WebKitGTK. Although since adopted by the GNOME project, it originally was developed by the Yorba Foundation. The purpose of this e-mail client, according to Adam Dingle, Yorba founder, was to bring back users from online webmails to a faster and easier to use desktop application.

Pantheon Mail was a fork initiated by the Elementary OS community after the demise of Yorba, though it was later rewritten from scratch so that the only remaining references to Geary in the Pantheon code base are in some translations.