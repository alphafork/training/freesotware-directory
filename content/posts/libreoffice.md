---
title: "Libreoffice"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

LibreOffice is a free and open-source office productivity software suite, a project of The Document Foundation (TDF). It was forked in 2010 from OpenOffice.org, an open-sourced version of the earlier StarOffice. The LibreOffice suite consists of programs for word processing, creating and editing of spreadsheets, slideshows, diagrams and drawings, working with databases, and composing mathematical formulas. It is available in 115 languages. TDF does not provide support for LibreOffice, but enterprise-focused editions are available from companies in the ecosystem.




