---
title: "LibreCAD"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction


LibreCAD is a computer-aided design (CAD) application for 2D design. It is free and open-source, and available for Linux, macOS, and Windows operating systems.

Most of the interface and handle concepts are analogous to AutoCAD, making it easier to use for users with experience in this type of commercial CAD application.



