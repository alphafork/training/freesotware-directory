---
title: "Audacious"
date: 2023-10-07:03:20-08:00
draft: true
---
## Introduction

Audacious is a free and open-source audio player software with a focus on low resource use, high audio quality, and support for a wide range of audio formats.[6] It is designed primarily for use on POSIX-compatible Unix-like operating systems, with limited support for Microsoft Windows.[7] Audacious was the default audio player in Ubuntu Studio in 2011-12,[8][9] and was the default music player in Lubuntu until October 2018, when it was replaced with VLC.[10]
