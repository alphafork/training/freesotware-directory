---
title: "Mariadb"
date: 2023-10-07T09:03:20-08:00
draft: true
---
## Introduction

MariaDB is a community-developed, commercially supported fork of the MySQL relational database management system (RDBMS), intended to remain free and open-source software under the GNU General Public License. Development is led by some of the original developers of MySQL, who forked it due to concerns over its acquisition by Oracle Corporation in 2009

Visit the [Hugo](https://gohugo.io) website!

